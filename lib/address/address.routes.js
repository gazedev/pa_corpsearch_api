const Joi = require('@hapi/joi');

module.exports = {
  routes: (models) => {
    const helpers = require('../helpers');
    const controllers = require('./address.controllers.js')(models);
    const addressModels = require('./address.models');

    return [
      {
        method: 'POST',
        path: '/addresses/geocode',
        handler: controllers.geocodeAddresses,
        options: {
          auth: 'jwt',
          pre: [
            { method: helpers.ensureAdmin }
          ],
          description: 'Geocode Addresses',
          notes: 'Process Addresses and Geocode the corporation details addresses',
          tags: ['api', 'Addresses'],
          validate: {
            query: addressModels.apiGeocodeQuery,
          }
        }
      },
      {
        method: 'GET',
        path: `/addresses/bounds`,
        handler: controllers.getAddressesFromBounds,
        options: {
          auth: false,
          description: 'Get an array of corporation addresses that is currently within the bounds of the map',
          notes: 'This endpoint searches for corporation addresses that are contained within the geometric boundaries',
          tags: ['api', 'Addresses'],
          validate: {
            query: Joi.object().keys({
              minLat: Joi.number().min(-90).max(90).required().less(Joi.ref('maxLat')),
              maxLat: Joi.number().min(-90).max(90).required(),
              minLng: Joi.number().min(-180).max(180).required().less(Joi.ref('maxLng')),
              maxLng: Joi.number().min(-180).max(180).required(),
            })
          }
        }
      },
    ];
  },
};
