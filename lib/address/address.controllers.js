module.exports = (models) => {
  const Boom = require('@hapi/boom');
  const https = require('https');
  const Sequelize = require('sequelize');
  const Op = Sequelize.Op;
  const {preProcessAddress} = require('../corp/corp.helpers');


  // const EventEmitter = require('events');

  // class SearchEmitter extends EventEmitter {}
  // const searchEmitter = new SearchEmitter();

  return {
    geocodeAddresses: async function(request, h) {
      console.time('geocodeAddresses');
      // query params: fromCorpId, toCorpId, addressILike
      const fromCorpId = request.query.fromCorpId;
      const toCorpId = request.query.toCorpId;
      const addressILike = request.query.addressILike;

      // TODO: it would also be nice if we could visualize this in the UI like we do we corp searches.
      console.time('getAddresses');
      const addresses = await models.Address.findAll({
        attributes: ['id', 'CorpId', 'address'],
        where: {
          address: {
            [Op.iLike]: addressILike,
          },
          CorpId: {
            [Op.gte]: fromCorpId,
            [Op.lte]: toCorpId,
          },
        },
        order: [['CorpId', 'ASC']],
        raw: true,
      })
      .then((data) => {
        return data.map(item => {
          item.processedAddress = preProcessAddress(item.address, true, false);
          return item;
        });
      });
      console.timeEnd('getAddresses');

      console.time('splitSpanIntoIterativeParallizedQueuesBatch');
      const response = splitSpanIntoIterativeParallizedQueuesBatch({
        start: 0,
        end: addresses.length -1,
        queues: 2,
        iterationFunction: async ({queueIndex, id}) => {
          const address = addresses[id];
          console.log("sending request to geocode api")
          // turns out some addresses match less when they are processed...
          // like 112 Point Breeze Place becomes 112 Pt Breeze Pl and matches less
          console.time('addressGeocodeAPI: ' + address.id);
          await addressGeocodeAPI(address.processedAddress).then((geocode) => {
            if (!geocode.results.length) {
              console.log("no results for address", address.processedAddress);
              return null;
            }
            const highestResult = geocode.results[0];
            if (highestResult.confidence < 4.2) {
              console.log("low confidence for address", address.processedAddress, "confidence", highestResult.confidence, "result", highestResult);
              return null;
            }
            console.log("updating address", address.id, "with geocode", highestResult);
            const updateAddress = models.Address.update({
              highestGeocoderResult: highestResult,
              geocodedAddress: highestResult.components,
              geometry: {
                type: 'Point',
                coordinates: [
                  highestResult.geometry.lng,
                  highestResult.geometry.lat,
                ],
              },
              geocoderConfidence: highestResult.confidence,
            }, {
              where: {
                id: address.id,
              }
            });
            return updateAddress;
          });
          console.timeEnd('addressGeocodeAPI: ' + address.id);
          return;

        }
      });
      console.timeEnd('splitSpanIntoIterativeParallizedQueuesBatch');
      console.timeEnd('geocodeAddresses');

      return response;
    },
    getAddressesFromBounds: async function(request, h) {
      const addresses = await getAddressesFromBounds(request.query.minLng, request.query.minLat, request.query.maxLng, request.query.maxLat);
      let mapAddresses = []

      for (let i = 0; i < addresses.length; i++) {
        mapAddresses.push({
          'type': 'Feature',
          'properties': {
            'source': addresses[i].source,
            'CorpId': addresses[i].CorpId
          },
          'geometry': {
            'type': addresses[i].geometry.type,
            'coordinates': addresses[i].geometry.coordinates
          }
        })
      }

      return featureCollection(mapAddresses);
    },
  };

  async function getAddressesFromBounds(minLat, minLng, maxLat, maxLng) {
    const sequelize = models.sequelize;
    const Address = models.Address;
    let mapAddresses = await Address.findAll({
      attributes: { exclude: ['id'] },
      where: sequelize.where(
        sequelize.fn(
          'ST_Covers',
          sequelize.fn('ST_GeomFromText', `POLYGON((${minLat} ${minLng}, ${minLat} ${maxLng}, ${maxLat} ${maxLng}, ${maxLat} ${minLng}, ${minLat} ${minLng}))`, '4326'),
          sequelize.col('geometry')
        ),
        'TRUE')
    });
    return mapAddresses;
  };

};

function featureCollection(features = []) {
  return {
    'type': 'FeatureCollection',
    'features': features
  };
}

function addressGeocodeAPI(address) {
  const url = require('url');
  const api = new URL(process.env.ADDRESS_API);

  let https;
  if (api.protocol == 'http:') {
    https = require('http');
  } else {
    https = require('https');
  }

  api.searchParams.set('q', address);

  return new Promise((resolve, reject) => {

    https.get(api, res => {
      res.setEncoding("utf8");
      let body = "";
      res.on("data", data => {
        body += data;
      });
      res.on("end", () => {
        try {
          body = JSON.parse(body);
        } catch (error) {
          console.error("error parsing body:", body);
          reject(error);
        }
        resolve(body);
      });
      res.on('error', error => {
        reject(error);
      });
    });

  });
}

// make a generalized queue function that takes args and a function to execute on each item
function splitSpanIntoIterativeParallizedQueuesBatch({
  start,
  end,
  queues,
  iterationFunction,
}) {
  try {
    let diff = end - start;
    let count = diff + 1; // because 'end' is inclusive
    let queueSize = Math.floor(count/queues);
    let maxQueueSize = Math.ceil(count/queues);
    let largerQueues = count % queues;
    // fill queues with sequential ids. fill as many queues to the max as possible
    // start queues async. each queue processes its items sync
    let enumStart = start;
    for (let queueIndex = 0; queueIndex < queues; queueIndex ++) {
      let isMaxQueue = (largerQueues - queueIndex > 0);
      let currentQSize = isMaxQueue ? maxQueueSize : queueSize;
      let enumEnd = enumStart + currentQSize -1;
      // we want to start all queues async, so don't await them. each queue
      // itself will run sync though
      processSynchronousParallizedQueue({
        queueIndex: queueIndex,
        start: enumStart,
        end: enumEnd,
        iterationFunction,
      })
      console.log("call processQueueSync", {queue: queueIndex, start: enumStart, end: enumEnd});

      enumStart = enumEnd + 1;
    }

    return {
      results: `Started query of entities ${start} - ${end} using ${queues} queue(s)`,
    };
  } catch (e) {
    console.log('error', e);
    throw "Request Failed";
  }
}

async function processSynchronousParallizedQueue({
  queueIndex,
  start,
  end,
  iterationFunction,
}) {
  let id;
  return new Promise(async (resolve, reject) => {
    try {
      for (let i = start; i <= end; i ++) {
        // we save a copy of id so when we use it for logging we don't get an
        // incremented version of the iterator
        id = i;
        console.log("about to call iterationFunction")
        await iterationFunction({queueIndex, id});
        console.log("iteration function has finished")
        // inside the loop
      }
      // after the loop
    } catch (e) {
      // catch
      console.log('queue failure', e);
      reject(e);
    } finally {
      // cleanup
    }
    resolve(true);
  });

}
