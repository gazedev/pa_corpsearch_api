const Joi = require('@hapi/joi');

module.exports = {
  db: (sequelize, DataTypes) => {
    const Address = sequelize.define('Address', {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      source: DataTypes.ENUM('details', 'officers'),
      position: DataTypes.INTEGER,
      address: DataTypes.STRING,
      searchableAddressV1: DataTypes.STRING,
      highestGeocoderResult: {
        type: DataTypes.JSONB,
      },
      geocodedAddress: {
        type: DataTypes.JSONB,
      },
      geometry: {
        type: DataTypes.GEOMETRY,
      },
      geocoderConfidence: {
        type: DataTypes.DECIMAL,
      },
    });

    Address.associate = function (models) {
      models.Address.belongsTo(models.Corp, {foreignKey: 'CorpId'});
      models.Corp.hasMany(models.Address, {foreignKey: 'CorpId'});
    };

    return Address;
  },
  apiGeocodeQuery: Joi.object().keys({
    // fromCorpId, toCorpId, addressILike
    fromCorpId: [
      Joi.number().integer().min(1).required(),
    ],
    toCorpId: [
      Joi.number().integer().greater(Joi.ref('fromCorpId')).required(),
    ],
    addressILike: [
      Joi.string().required(),
    ],
  }),
};
