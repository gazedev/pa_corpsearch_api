module.exports = (models) => {
  const Boom = require('@hapi/boom');
  const https = require('https');
  // const used = process.memoryUsage().heapUsed / 1024 / 1024;
  // console.log(`The script uses approximately ${Math.round(used * 100) / 100} MB`);
  const Sequelize = require('sequelize');
  const Op = Sequelize.Op;
  const CDP = require('chrome-remote-interface');
  const cdpOptions = {
    host: 'chrome'
  };

  const EventEmitter = require('events');

  class SearchEmitter extends EventEmitter {}
  const searchEmitter = new SearchEmitter();

  return {
    postCorpsEnumeration: async function(request, h) {
      try {
        let start = request.query.start;
        let end = request.query.end;
        let queues = request.query.queues;
        let diff = end - start;
        let count = diff + 1; // because 'end' is inclusive
        let queueSize = Math.floor(count/queues);
        let maxQueueSize = Math.ceil(count/queues);
        let largerQueues = count % queues;

        let dbStart = start;
        let dbQueues = {};
        for (let qI = 0; qI < queues; qI ++) {
          let isMaxQueue = (largerQueues - qI > 0);
          let currentQSize = isMaxQueue ? maxQueueSize : queueSize;
          let dbEnd = dbStart + currentQSize -1;
          dbQueues[qI] = {
            status: 'in-progress',
            start: dbStart,
            end: dbEnd,
            id: dbStart,
          };
          dbStart = dbEnd + 1;
        }
        let search = await models.Search.create({
          query: {
            start: request.query.start,
            end: request.query.end,
            queues: request.query.queues,
          },
          queues: dbQueues,
        });

        // fill queues with sequential ids. fill as many queues to the max as possible
        // start queues async. each queue processes its items sync
        let enumStart = start;
        for (let qI = 0; qI < queues; qI ++) {
          let isMaxQueue = (largerQueues - qI > 0);
          let currentQSize = isMaxQueue ? maxQueueSize : queueSize;
          let enumEnd = enumStart + currentQSize -1;
          // we want to start all queues async, so don't await them. each queue
          // itself will run sync though
          processQApiSync({search: search, queue: qI, start: enumStart, end: enumEnd});

          enumStart = enumEnd + 1;
        }

        return {
          query: request.query,
          results: `Started query of entities ${request.query.start} - ${request.query.end} using ${queues} queue(s)`,
        };
      } catch (e) {
        console.log('error', e);
        throw "Request Failed";
      }
    },
    resumeCorpsEnumeration: async (request, h) => {
      let searchId = request.params.searchId;
      let search;
      try {
        search = await models.Search.findByPk(searchId);
        if (search === null) {
          throw "No result for that search ID";
        }
      } catch (e) {
        console.log('Error getting search', e);
      }

      for (let qI = 0; qI < search.query.queues; qI ++) {
        if (search.queues[qI].hasOwnProperty('targetId')) {
          try {
            let closeOptions = cdpOptions;
            closeOptions.id = search.queues[qI].targetId;
            console.log('resuming search. closing tab with id', closeOptions.id);
            await CDP.Close(closeOptions);
          } catch (e) {
            console.log('Error closing tab with id ', search.queues[qI].targetId, e);
          }
        }
        let enumStart = search.queues[qI].id;
        let enumEnd = search.queues[qI].end;
        processQApiSync({search: search, queue: qI, start: enumStart, end: enumEnd});
      }

      return {
        results: `Resumed search with id ${searchId}`,
      };
    },
    actionOnCorpsEnumeration: async (request, h) => {
      let searchId = request.params.searchId;
      let action = request.payload.action;
      let search = request.pre.search;

      if (action === 'stop' || action === 'resume') {
        for (let qI = 0; qI < search.query.queues; qI ++) {
          if (search.queues[qI].hasOwnProperty('targetId')) {
            try {
              let closeOptions = cdpOptions;
              closeOptions.id = search.queues[qI].targetId;
              await CDP.Close(closeOptions);
            } catch (e) {
              console.error('Error closing tab with id ', search.queues[qI].targetId, e);
            }
          }
          if (action === 'resume') {
            let enumStart = search.queues[qI].id;
            let enumEnd = search.queues[qI].end;
            processQApiSync({search: search, queue: qI, start: enumStart, end: enumEnd});
          }
        }
      }

      return {
        results: `Action (${action}) done for search with id ${searchId}`,
      };
    },
    getSearchEvents: (request, h) => {
      let { ws } = request.websocket();
      ws.send(JSON.stringify({ cmd: "HELLO", arg: "OPEN"}));
      searchEmitter.on('event', (params) => {
        ws.send(JSON.stringify(params));
      });
      return "";
    },
    getCorpsById: async function(request, h) {
      let corpId = request.params.CorpId;
      let corp = await models.Corp.findByPk(corpId);
      return corp;
    },
    getCorpsSearch: async function(request, h) {
      const { preProcessAddress, preProcessName } = require('./corp.helpers.js');
      let queryLimit = 8000;
      let idsBuilder = {};

      if (propOf(request.query, 'address')) {
        let address = request.query.address;
        let queryCorpIds = models.Address.findAll({
          attributes: ['CorpId'],
          where: {
            searchableAddressV1: {
              [Op.iLike]: '%'+ preProcessAddress(address, false) +'%',
            }
          },
          limit: queryLimit,
          order: [['CorpId', 'DESC']],
          raw: true,
        }).then((data) => {
          return data.map(item => item.CorpId);
        });
        idsBuilder.address = queryCorpIds;
      }

      if (propOf(request.query, 'name')) {
        let name = request.query.name;
        let queryCorpIds = models.Name.findAll({
          attributes: ['CorpId'],
          where: {
            searchableNameV1: {
              [Op.iLike]: '%'+ preProcessName(name, false) +'%',
            }
          },
          limit: 8000,
          order: [['CorpId', 'DESC']],
          raw: true,
        }).then((data) => {
          return data.map(item => item.CorpId);
        });
        idsBuilder.name = queryCorpIds;
      }

      // wait for both queries so they run parallel, not sequentially
      let queries = await Promise.all([idsBuilder.address, idsBuilder.name]);

      let notices = [];
      if (idsBuilder.hasOwnProperty('name')) {
        // should already be resolved, so negligble promise extraction
        let nameIds = await idsBuilder.name;
        if (nameIds.length === queryLimit) {
          notices.push({
            message: "Name query results truncated",
          });
        }
      }

      if (idsBuilder.hasOwnProperty('address')) {
        // should already be resolved, so negligble promise extraction
        let addressIds = await idsBuilder.address;
        if (addressIds.length === queryLimit) {
          notices.push({
            message: "Address query results truncated",
          });
        }
      }

      let corpIds = [];
      for (let queryIds of queries) {
        corpIds = corpIds.concat(queryIds);
      }

      let results;
      try {
        results = await models.Corp.findAll({
          where: {
            'id': {
              [Op.in]: corpIds,
            }
          },
          order: [['id', 'DESC']]
        });
      } catch (e) {
        console.error('Error', e);
      }

      return h.response({
        query: request.query,
        count: results.length,
        notices: notices,
        results: results,
      });

    },
    processCorpsSearchables: async function(request, h) {
      let start = request.payload.start;
      let end;
      if (request.payload.hasOwnProperty('end')) {
        end = request.payload.end;
      } else {
        end = start;
      }
      let limit = 100000;
      while (start <= end) {
        let tempEnd;
        if (start + limit -1 < end) {
          tempEnd = start + limit -1;
        } else {
          tempEnd = end;
        }
        await processAndUpdateSearchables(start, tempEnd);
        start = tempEnd +1;
      }

      return h.response({
        query: request.payload,
        count: end - request.payload.start +1,
      });
    },
    lib: {
      ensureSearch: ensureSearch,
    }
  };

  function propOf(obj, prop) {
    return Object.prototype.hasOwnProperty.call(obj, prop);
  }

  // business search api returns an object of records. We should find the one with the RECORD_NUM matching what we asked for
  async function requestToRecordsBusinessSearchApi(entityId) {

    const records = await new Promise((resolve, reject) => {
      var http = require("https");
  
      const postData = JSON.stringify({
        "SEARCH_VALUE": `${entityId}`,
        "SEARCH_FILTER_TYPE_ID": "1",
        "FILING_TYPE_ID": "",
        "STATUS_ID": "",
        "FILING_DATE": {
          "start": null,
          "end": null
        },
      });

      const url = "https://file.dos.pa.gov/api/Records/businesssearch";

      const options = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Content-Length': Buffer.byteLength(postData)
        },
      };
  
      var req = http.request(url, options, function (res) {
        let chunks = "";
  
        res.on("data", function (chunk) {
          chunks += chunk;
        });
  
        res.on("end", function () {
          resolve(JSON.parse(chunks));
        });
      });
  
      req.write(
        postData
      );
      req.end();
  
    });

    // we need to access records.rows, which is an object. 
    // We need to iterate the object keys and find the one 
    // that has a RECORD_NUM that when converted to a number 
    // matches the entityId
    let record = null;
    for (let key of Object.keys(records.rows)) {
      if (parseInt(records.rows[key].RECORD_NUM) === entityId) {
        record = records.rows[key];
        break;
      }
    }

    return record;
  }

  async function requestToFilingDetailsApi(entityId) {
    // const options = {
    //   url: `https://www.corporations.pa.gov/search/corpsearch?corps=${entityId}`,
    // };
    // this needs to actually not use the entityId (record number), but the id from the business search
    return new Promise((resolve, reject) => {
      https.get(`https://file.dos.pa.gov/api/FilingDetail/business/${entityId}/false`, (resp) => {
        let data = '';
        resp.on('data', (chunk) => {
          data += chunk;
        });
        resp.on('end', () => {
          resolve(JSON.parse(data));
        });
      }).on("error", (err) => {
        reject(err);
      });
    });
  }

  async function requestToHistoryApi(entityId) {
    return new Promise((resolve, reject) => {
      https.get(`https://file.dos.pa.gov/api/History/business/${entityId}`, (resp) => {
        let data = '';
        resp.on('data', (chunk) => {
          data += chunk;
        });
        resp.on('end', () => {
          resolve(JSON.parse(data));
        });
      }).on("error", (err) => {
        reject(err);
      });
    });
  }

  async function processQApiSync(params) {
    /*
      params: {
        search: ,
        queue: ,
        start: ,
        end: ,
      }
    */
    let id;
    return new Promise(async (resolve, reject) => {
      try {
        for (let i = params.start; i <= params.end; i ++) {
          // we save a copy of id so when we use it for logging we don't get an
          // incremented version of the iterator
          id = i;
          console.log('requesting id', id);
          let result = await singleEntityApiRequest(id);
          if (result === null) {
            models.Corp.upsert({
              id: id,
            }).then(inserted => {
              console.log(`undefined was inserted: `, id);
            }).catch(e => {
              console.log('upsert caught', e);
            });
          } else {
            console.log("upserting result", result);
            models.Corp.upsert({
              id: id,
              name: result.name,
              namesHistory: result.namesHistory,
              details: result.details,
              officers: result.officers,
              rawApiData: result.rawApiData,
            }).then(inserted => {
              // console.log(`entity was inserted`);
            }).catch(e => {
              console.log('upsert caught', e);
            });
          }
          // inside the loop
          searchEmitter.emit('event', {search: params.search.id, start: params.start, end: params.end, queue: params.queue, status: 'in-progress', id: id});
          // although in-progress and isn't really that dynamic we
          // set it here because we have access to it here and if for some
          // reason it did change we can be sure it'll be kept current here
          await params.search
            .set(`queues.${params.queue}.status`, 'in-progress')
            .set(`queues.${params.queue}.id`, id)
            .save();
        }
        // after the loop
        searchEmitter.emit('event', {search: params.search.id, start: params.start, end: params.end, queue: params.queue, status: 'complete', id: id});
        await params.search
          .set(`queues.${params.queue}.status`, 'complete')
          .set(`queues.${params.queue}.id`, id)
          .save();
      } catch (e) {
        // catch
        searchEmitter.emit('event', {search: params.search.id, start: params.start, end: params.end, queue: params.queue, status: 'error', id: id});
        await params.search
          .set(`queues.${params.queue}.status`, 'error')
          .set(`queues.${params.queue}.id`, id)
          .save();

        console.log('queue failure', e);
        reject(e);
      } finally {
        // cleanup
      }
      resolve(true);
    });

  }


  // handles the request and the retry logic
  async function singleEntityApiRequest(recordNumber) {
    let resultData;
    try {
      // search for the entity based on enumerable id
      const record = await requestToRecordsBusinessSearchApi(recordNumber);
      
      if (record === null) {
        return null;
      }
      const id = record.ID;

      // we take the ID from the business search result and use that to get FilingDetail/business
      const details = await requestToFilingDetailsApi(id);

      // we use record.RECORD_NUM as it is properly left padded with 0s
      const history = await requestToHistoryApi(record.RECORD_NUM);

      const name = extractCleanName(record);

      const extractedNamesHistory = extractNamesHistory(history);

      const extractedDetails = extractAndFormatDetails({name, record, details});

      const officers = extractOfficers(details);

      // ensure name is part of the namesHistory, and deduplicate the namesHistory, 
      const namesHistory = [...new Set([name, ...extractedNamesHistory])].map((value, index) => {
        if (index === 0) {
          return {
            "Current Name": value,
          };
        }
        return {
          "Prior Name": value,
        };
      });

      resultData = {
        name,
        namesHistory,
        officers,
        details: extractedDetails,
        rawApiData: {
          record,
          details,
          history,
        },
      };
    } catch (e) {
      console.log('error in singleEntityApiRequest', e);
    }
    return resultData;
  }

  function extractCleanName(record) {
    const recordNumSuffix = ` (${parseInt(record.RECORD_NUM)})`;
    const originalName = record.TITLE[0];
    // remove the recordNumSuffix from the end of originalName if it exists
    const name = originalName.endsWith(recordNumSuffix) ? originalName.slice(0, -recordNumSuffix.length) : originalName;
    return name;
  }

  function extractNamesHistory(history) {
    const historyList = history.HISTORY_LIST;
    let namesHistory = [];
    historyList.forEach((item) => {
      const startsWithNameChangedRegex = /^Name Change:/i;
      const isLegacyCommentRegex = /Legacy Comment/i;
      // we allow it through the filter if the FIELD_NAME is 'FILING_NAME' and the CHANGED_TO is not 'BName',
      // OR if the DISPLAY_NAME is 'Legacy Comment' and the CHANGED_TO starts with 'Name Change:'
      if (item.FIELD_NAME === 'FILING_NAME' && item.CHANGED_TO !== 'BName' && item.CHANGED_TO !== 'Not Applicable') {
        namesHistory.push(item.CHANGED_TO.trim());
      } else if(isLegacyCommentRegex.test(item.DISPLAY_NAME) && startsWithNameChangedRegex.test(item.CHANGED_TO)) {
        const cleanedName = item.CHANGED_TO.replace(startsWithNameChangedRegex, '').trim();
        namesHistory.push(cleanedName);
      }
    });
    return namesHistory;
  }

  function extractAddress(details) {
    const detailsList = details.DRAWER_DETAIL_LIST;
    console.log("detailsList", detailsList);
    const registeredOffices = detailsList.filter((item) => item.LABEL === 'Registered Office');
    const principalAddresses = detailsList.filter((item) => item.LABEL === 'Principal Address');
    let foundAddress;
    if (registeredOffices.length) {
      foundAddress = registeredOffices[0];
    } else if (principalAddresses.length) {
      foundAddress = principalAddresses[0];
    } else {
      return null;
    }
    const addressParts = foundAddress.VALUE.split('\n\n').filter((item) => item !== '');
    const oneOrMoreWhitespaces = /\s+/g;
    const address = addressParts.map(x => x.trim()).join(' ').replace(oneOrMoreWhitespaces, ' ');
    return address;
  }

  function extractAndFormatDetails({name, record, details}) {
    const extractedAddress = extractAddress(details);
    return {
      Name: name,
      Status: record.STATUS,
      Address: extractedAddress,
      Citizenship: record.ENTITY_TYPE.split(' ')[0],
      "Entity Type": record.ENTITY_TYPE.split(' ').slice(1).join(' '),
      "State of Inc:": record.FORMED_IN,
      "Entity Number": parseInt(record.RECORD_NUM).toString(),
      "Effective Date": record.FILING_DATE,
      // "Entity Creation Date": "",
    }
  }

  function extractOfficers(details) {
    const officersObject = details.DRAWER_DETAIL_LIST.filter((item) => item.LABEL === 'Officers');
    if (!officersObject.length) {
      return null;
    }
    const officersObjectValue = officersObject[0].VALUE;
    const officersSplit = officersObjectValue.split('\n\n').filter((item) => item !== '');
    const officers = [];
    officersSplit.forEach((officer) => {
      const [title, name, ...addressParts] = officer.split('\n');
      const oneOrMoreWhitespaces = /\s+/g;
      const address = addressParts.map(x => x.trim()).join(' ');
      officers.push({
        title: title.replace(oneOrMoreWhitespaces, ' '),
        name: name.replace(oneOrMoreWhitespaces, ' '),
        address: address.replace(oneOrMoreWhitespaces, ' '),
      });
    });
    return officers;
  }

  async function processAndUpdateSearchables(start, end) {
    const { preProcessAddress, preProcessName } = require('./corp.helpers.js');
    let getParams = {
      start: start,
      end: end,
    };
    // do a getAll of the corps between start and end
    // we're going to process them as a batch, generating a new array of names and addresses for the corps
    // we're going to try and do a bulk upsert of the names and addresses into the names and addresses tables
    // the thing is, we basically need to completely rewrite all names and addresses for that range of corps
    // not so much a problem for the first run, but in later runs, we don't want to get orphaned/out of date info for corps that have changed
    // let's do this by doing a transaction where we delete all names and addresses for that range of corps
    // and then we'll do a bulk create for the array of names and addresses

    // This will lead to new name and address ids being generated. Is this a problem?
    // should name and address have their own id, or should it just be a cumulative key of CorpId, type, position?
    // They will have their own guid for now.
    let addresses = [];
    let names = [];
    try {
      console.time('getCorps');
      let corps = await getCorpsForSearchables(getParams);
      console.timeEnd('getCorps');
      console.log('corps length', corps.length);

      console.time('processCorps');
      for (let corp of corps) {
        let corpId = corp.id;
        if (corp.details !== null && corp.details.hasOwnProperty('Address') && corp.details['Address']) {
          let address = corp.details['Address'];
          addresses.push({
            CorpId: corpId,
            source: 'details',
            address: corp.details['Address'],
            searchableAddressV1: preProcessAddress(address),
          });
        }
        if (corp.namesHistory !== null) {
          for (let i = 0; i < corp.namesHistory.length; i++) {
            let nameObj = corp.namesHistory[i];
            // namesHistory objects have one property, the value being the name
            let nameKey = Object.getOwnPropertyNames(nameObj)[0]
            let name = nameObj[nameKey];
            names.push({
              CorpId: corpId,
              source: 'namesHistory',
              position: i,
              name: name,
              searchableNameV1: preProcessName(name),
            });
          }
        }
        if (corp.officers !== null) {
          for (let i = 0; i < corp.officers.length; i++) {
            let officer = corp.officers[i];
            if (officer.hasOwnProperty('Address')) {
              let address = officer['Address']
              addresses.push({
                CorpId: corpId,
                source: 'officers',
                position: i,
                address: address,
                searchableAddressV1: preProcessAddress(address),
              });
            }
            if (officer.hasOwnProperty('Name')) {
              let name = officer['Name'];
              names.push({
                CorpId: corpId,
                source: 'officers',
                position: i,
                name: name,
                searchableNameV1: preProcessName(name),
              });
            }
          }
        }
      }
      console.timeEnd('processCorps');
    } catch (e) {
      console.log('error', e);
      throw "Request Failed";
    }

    console.time('clearAndInsert');
    try {
      let addressAction = await models.sequelize.transaction(async (t) => {
        let addressesDelete = await models.Address.destroy({
          where: {
            CorpId: {
              [Op.gte]: start,
              [Op.lte]: end,
            }
          }
        });
        let addressesCreate = await models.Address.bulkCreate(addresses);
      });
      let nameAction = await models.sequelize.transaction(async (t) => {
        let namesDelete = await models.Name.destroy({
          where: {
            CorpId: {
              [Op.gte]: start,
              [Op.lte]: end,
            }
          }
        });
        let namesCreate = await models.Name.bulkCreate(names);
      });
    } catch (e) {
      console.log('transaction error');
      console.log(e);
    }
    console.timeEnd('clearAndInsert');

    return;

  }

  function getCorpsForSearchables(params) {
    let start = params.start;
    let end = params.end;
    // get corps with id between (and including) start-end, where the name is
    // not null, and either the namesHistory, details or officers is not null
    return models.Corp.findAll({
      where: {
        [Op.and]: {
          id: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
          [Op.or]: {
            namesHistory: {[Op.not]: null},
            details: {[Op.not]: null},
            officers: {[Op.not]: null},
          }
        }
      },
      raw: true,
    });
  }

  async function ensureSearch(request) {
    let searchId = request.params.searchId;
    let search;
    try {
      search = await models.Search.findByPk(searchId);
    } catch (e) {
      throw Boom.badImplementation('Error during models.Search.findByPk(searchId).', e);
    }
    if (search === null) {
      throw "No result for that search ID";
    }
    return search;
  }

};
