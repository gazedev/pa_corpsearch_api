const { expect } = require('@hapi/code');
const Lab = require('@hapi/lab');
const lab = exports.lab = Lab.script();

const { formatMailingResults, formatMortgageResults } = require("./browser.helpers");

lab.experiment('Browser', () => {



  lab.test('Format a Mailing Result', async () => {
    const result = formatMailingResults("900 BELLAIRE AVE \nPITTSBURGH , PA 15226-1844"); 

    expect(result).to.equal({
      original: "900 BELLAIRE AVE \nPITTSBURGH , PA 15226-1844",
      cleaned: "900 BELLAIRE AVE PITTSBURGH, PA 15226-1844",
      parsed: {
        number: "900",
        street: "BELLAIRE",
        type: "Ave",
        city: "PITTSBURGH",
        state: "PA",
        zip: "15226",
        plus4: "1844"
      }
    });
  });

  lab.test('Format a Mortgage Result', async () => {
    const result = formatMortgageResults("KHALIL RAJA YOUSSEF\n900 BELLAIRE AVE\nPITTSBURGH PA\n15226-1844\n"); 

    expect(result).to.equal({
      original: "KHALIL RAJA YOUSSEF\n900 BELLAIRE AVE\nPITTSBURGH PA\n15226-1844\n",
      cleaned: "900 BELLAIRE AVE PITTSBURGH PA 15226-1844",
      parsed: {
        number: "900",
        street: "BELLAIRE",
        type: "Ave",
        city: "PITTSBURGH",
        state: "PA",
        zip: "15226",
        plus4: "1844"
      }
    });
  });


});

