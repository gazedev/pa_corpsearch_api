const Joi = require('@hapi/joi');

module.exports = {
  apiTabsId: Joi.object().keys({
    tabId: Joi.string().required(),
  }),
  ownerInfo: Joi.object().keys({
    parcelId: Joi.string().alphanum().min(16).max(16),
  })
};
