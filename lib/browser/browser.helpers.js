const Parser = require('parse-address');

function formatMailingResults(mailingOriginal, addressee) {
    const cleaned = mailingOriginal
        // convert newline to comma space
        .replace(/\n/g, ', ')
        // convert whitespaces (space, tabs, newline) to a single space
        .replace(/\s+/g, ' ')
        // clean up extra spacing before commas
        .replace(/ , /g, ', ')
        // remove trailing whitespace
        .replace(/\s+$/, '');
        
    let parsed = Parser.parseLocation(cleaned);
    parsed.addressee = addressee;
  return {    
    original: mailingOriginal,
    cleaned: cleaned,
    parsed: parsed,
  };
}

function formatMortgageResults(mortgageOriginal) {
    let isPoBox = false;

    // Set the first line of mortgageOriginal as addresssee
    const [first, ...rest] = mortgageOriginal.split('\n');
    let addressee = first;
    let mort = rest.join('\n');

    let cleaned = mort   
        // convert newline to comma space
        .replace(/\n/g, ', ')
        // clean up extra spacing before commas
        .replace(/ , /g, ', ')
        .replace(/\s+/g, ' ')
        // remove trailing whitespace
        .replace(/\s+$/, '');

    // process if returned data contains PO Box
    if(cleaned.includes(' PO BOX ')) {
        isPoBox = true;
        let poBoxSplit = cleaned.split(' PO BOX ')
        addressee += ' ' + poBoxSplit[0];

        let poBoxNumber =  poBoxSplit[1];
        cleaned = 'PO BOX ' + poBoxNumber;
    }

    let parsed = Parser.parseLocation(cleaned);
    parsed.addressee = addressee;

    // parse-address Parser returns PO Box as sec_unit_type and sec_unit_num
    // TCVCOG wants that data in fields "street: and "number"
    if(isPoBox && parsed.hasOwnProperty("sec_unit_type") && parsed.hasOwnProperty("sec_unit_num")){
        parsed.street = parsed.sec_unit_type;
        parsed.number = parsed.sec_unit_num;
        delete parsed.sec_unit_type;
        delete parsed.sec_unit_num;
    }
    return {
        original: mortgageOriginal,
        cleaned: cleaned,
        parsed: parsed,
    }
  
}

module.exports = {
    formatMailingResults: formatMailingResults,
    formatMortgageResults: formatMortgageResults,
} 