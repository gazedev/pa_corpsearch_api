'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
   return queryInterface.addColumn('Addresses', 'highestGeocoderResult', Sequelize.JSONB);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Addresses', 'highestGeocoderResult');
  }
};
