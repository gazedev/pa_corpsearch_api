'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Addresses', 'geocoderConfidence', { type: Sequelize.DECIMAL });

  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Addresses', 'geocoderConfidence');
  }
};
