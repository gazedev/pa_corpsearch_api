'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query('CREATE EXTENSION IF NOT EXISTS postgis;');
    await queryInterface.addColumn('Addresses', 'geocodedAddress', Sequelize.JSONB);
    return queryInterface.addColumn('Addresses', 'geometry', { type: Sequelize.GEOMETRY });

  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('Addresses', 'geocodedAddress');
    return queryInterface.removeColumn('Addresses', 'geometry');
  }
};
