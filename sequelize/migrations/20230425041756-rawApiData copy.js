'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
   return queryInterface.addColumn('Corps', 'rawApiData', Sequelize.JSONB);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Corps', 'rawApiData');
  }
};
